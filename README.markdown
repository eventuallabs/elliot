# Elliot #

Communicate with the RTU-307C using Tcl and Modbus-RTU.  This builds
on the Tcl Modbus code at
[the Tcl wiki](https://wiki.tcl-lang.org/page/Modbus+PLC+Communications+Driver).

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Elliot](#elliot)
    - [Hardware overview](#hardware-overview)
        - [Module power](#module-power)
        - [Jumper settings](#jumper-settings)
        - [RS-232 or RS-485 connection](#rs-232-or-rs-485-connection)
        - [Output rise and fall times](#output-rise-and-fall-times)
        - [Output resistance](#output-resistance)
    - [Software installation](#software-installation)
        - [Get tksvg](#get-tksvg)
    - [Screenshot](#screenshot)
    - [Software overview](#software-overview)
        - [Connecting to a USB/RS-485 or USB/RS-232 device](#connecting-to-a-usbrs-485-or-usbrs-232-device)
            - [Adding a new approved RS-232 or RS-485 transceiver](#adding-a-new-approved-rs-232-or-rs-485-transceiver)
        - [Setting voltages with both sliders and entry boxes](#setting-voltages-with-both-sliders-and-entry-boxes)
    - [Modbus references](#modbus-references)

<!-- markdown-toc end -->


## Hardware overview ##

As of June, 2021, searching ebay for RTU-307C returned a few listings
of the 4-channel 0V ⟶ 10V module shown below. These sell for about 50
USD and communicate using both RS-232 and RS-485.  The USB/RS-485
converter shown is also an ebay item, listed for about 3 USD as "CH340
USB to RS485 converter."

![Hardware overview](img/rtu_307c_with_rs485.jpg)

### Module power ###

The module needs a 10V ⟶ 30V power supply.  I used a 12V, 24W wall transformer for this.

### Jumper settings ###

There are 4 jumpers under the cover used to configure the 4 analog
outputs for either 4mA ⟶ 20mA current output or 0V ⟶ 10V voltage
output.  Each of these jumpers must be inserted (shorting both pins)
for voltage output

### RS-232 or RS-485 connection ###

Connect Rx on your host interface to Tx on the module.  Connect Tx on
the host interface to Rx on the module.  The image above shows the
RTU-307C connected to a USB/RS-485 adaptor.

### Output rise and fall times ###

Typical 0V ⟶ 10V and 10V ⟶ 0V transitions are shown below.  The rise
and fall times for output channels are about 350ms.  You might wait
about 700ms after a transition to be sure about the new output value.

![Ch1 rise time](img/ch1_rise_time.png)

![Ch4 fall time](img/ch4_fall_time.png)

### Output resistance ###

The table below shows output voltages for a few different resistive
loads.  I calculate an output resistance of about 500Ω from these
values.

| Load resistance (Ω) | Output (V) |
| ------------------- | --------- |
| 1M | 9.97 |
| 100k | 9.92 |
| 10k | 9.5 |
| 1k | 6.66 |

## Software installation ##

You will, of course, need Tcl and Tk to run this.
[Ashok P. Nadkarni](https://www.magicsplat.com/tcl-installer/index.html)
has binary distributions for Windows.  Linux users can use
[the instructions here](https://www.tcl.tk/software/tcltk/).

After cloning this code and adding **tksvg** (see below), you can start the software with
```
tclsh main.tcl
```
...from the `src` directory.

### Get tksvg ###

The [tksvg](https://wiki.tcl-lang.org/page/tksvg) package needs to be
installed into the `src/lib` directory.  This supports images on
buttons.  There are releases for Linux and Windows
[on Github](https://github.com/oehhar/tksvg/releases). You'll need the
correct package for your operating system.  The path to the package
should end up looking like

```
elliot/src/lib/tksvg0.8
```
...or something similar depending on the
version.


## Screenshot ##

This screenshot shows the Elliot GUI next to the
[Yelloscope](https://gitlab.com/eventuallabs/yelloscope) GUI.  You can
see the **Synchronized** LED flashing red as the 9600 baud
communication channel struggles to keep up with my slider movement.

![Elliot with Yellowscope](img/elliot_with_yelloscope.gif)

## Software overview ##

### Connecting to a USB/RS-485 or USB/RS-232 device ###

Use the magnifying glass shown below to search for approved RS-232 or
RS-485 transceivers. Elliot will stop searching when it finds an approved device.

![elliot search](img/elliot_search.gif)

The **Status** LED won't change to green until Elliot is able to
connect to a Modbus station.  If Elliot doesn't find your transceiver,
you may need to add a new device to the ``approved_usb_devices.tcl``
file.

#### Adding a new approved RS-232 or RS-485 transceiver ####

The ``approved_usb_devices.tcl`` file contains all the UART
transcivers you'd like to use.  This is just a tcl script that gets
sourced by the main program.  Elliot needs the vendor ID (VID) and the
product ID (PID) for the device for searching.  The ``Vendor Name``
and ``Product Name`` fields just help you to remember the device.  As an example:

```
lappend usb::approved_device_list [usb::Device new 1a86 "QinHeng" 5523 "USB/RS-485 adaptor"]
```

...shows the line adding a QinHeng RS-485 transceiver.  This tells
Elliot to create a ``usb::Device`` object with a vendor ID of 0x1a86
and a product ID of 5523.  You can use
[a USB vendor ID list](http://www.linux-usb.org/usb.ids) to link these
IDs to names.

### Setting voltages with both sliders and entry boxes ###

A tricky piece of the GUI is making sure the user can set voltages
with both the entry boxes and the sliders. The flow diagram below
shows how Elliot uses four `channel_n_setting_v` variables to keep
track of settings coming from the entry boxes or sliders.  This
varible is read by `request_voltage_settings()`, which ultimately
calls the **RTU307C** object's `set_voltage()` method.

Slider updates happen faster than the hardware update rate, so the
`sync_led` turns red until the output voltages are stable.

![Voltage setting flow](img/voltage_setting_flow.png)


## Modbus references ##

* [Witte Software](https://www.modbustools.com/index.html) has a nice description of [function codes](https://www.modbustools.com/modbus.html#function06)
* [LibModbus](https://libmodbus.org/) is a Modbus communication library written in C.
  * I could have extended Tcl to call functions from this library
    instead of using pure-Tcl sample code.  That could be an
    interesting next project.
* [Jamod](http://jamod.sourceforge.net/) allows Modbus communication from Java.

