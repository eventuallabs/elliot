
# LEDs need fonts
main::source_script fonts.tcl


namespace eval leds {

    variable led_diameter 20

    

    proc create_led {canvas_widget color} {
	# Return a led object in the supplied canvas widget.  The LED
	# will fill the canvas.
	#
	# Arguments:
	#   canvas_widget -- The widget created to hold the LED
	#   color -- Color of the LED
	set offset 2
	set canvas_width [$canvas_widget cget -width]
	set x1 $offset
	set y1 $offset
	set x2 $canvas_width
	set y2 $canvas_width
	$canvas_widget create oval $x1 $y1 $x2 $y2 -fill $color -tags "led"
	return $canvas_widget
    }

    proc set_led_color {canvas_widget color} {
	# Change the color of the LED inside the canvas widget
	$canvas_widget itemconfigure led -fill $color
    }

    oo::class create LED {

	variable LED_canvas
	variable LED_label


	# This will hold the current color of the LED, and will
	# indicate things like on/off or enabled/disabled.
	variable Color
	
	constructor {window_path label_string} {
	    # Create the LED object
	    #
	    # Arguments:
	    #   window_path -- The Tk window path to contain the new LED canvas
	    #   label_string -- Used to create a label widget
	    #   on_color -- Color used in the on state
	    #   off_color -- Color used in the off state

	    # LEDs are just colored circles on a canvas.  Default color is red
	    set Color "red"

	    # Tcl gets confused by the :: put in default object names when you try to use things like cget
	    set self_string [string map {:: _} [string trim [self] :]]
	    
	    set LED_canvas [canvas "$window_path.${self_string}_canvas" \
				-width $leds::led_diameter -height $leds::led_diameter]

	    # LEDs will have a label widget
	    set LED_label [label "$window_path.${self_string}_label" \
			       -text $label_string -font fonts::led_font]
	    set offset 2
	    set canvas_width [$LED_canvas cget -width]
	    set x1 $offset
	    set y1 $offset
	    set x2 $canvas_width
	    set y2 $canvas_width


	    $LED_canvas create oval $x1 $y1 $x2 $y2 -fill $Color -tags "led"
	}

	# Remember that methods starting with a lowercase letter are
	# exported by default
	method get_canvas {} {
	    variable LED_canvas
	    return $LED_canvas
	}

	method get_label {} {
	    variable LED_label
	    return $LED_label
	}

	method set_color {color} {
	    variable LED_canvas
	    variable Color
	    set Color $color
	    $LED_canvas itemconfigure led -fill $color
	}

	method get_color {} {
	    variable Color
	    return $Color
	}

    }

    
}
