# For setting values on the RTU-307C Modbus RTU 4-channel analog output

namespace eval rtu307c {

    # A RTU-307C device from Gecon
    oo::class create RTU307C {

	# The object gets assigned an active communication channel when created
	variable Channel

	# The baud rate defaults to 9600
	variable Baud

	# Serial port parity settings:
	#
	# n -- none
	# o -- odd
	# e -- even
	# m -- mark
	# s -- space
	variable Parity

	# Timeout for messages (ms)
	variable Timeout_ms

	# Station number
	variable Station_number

	# Current list of voltage output values
	variable Output_voltage_list

	constructor {channel} {
	    set Channel $channel
	    set Baud 9600
	    set Parity n
	    set Output_voltage_list [list 0 0 0 0]
	    set Station_number $::root_window::station_number
	    set Timeout_ms 1000
	}

	method set_voltage { voltage_channel voltage_V } {
	    # Set one channel's output voltage
	    #
	    # Arguments:
	    #   voltage_channel -- Outputs 1 --> 4
	    #   voltage -- Output voltage (0 -- 10V)
	    global log
	    variable Channel
	    variable Output_voltage_list
	    variable Station_number
	    variable Timeout_ms

	    set voltage_dec [expr round(( double($voltage_V)/10 ) * 50000)]
	    set voltage_hex [format "%04X" $voltage_dec]

	    # Modbus addresses start at 0 instead of 1
	    set modbus_reg [expr $voltage_channel -1]

	    # Register writes always target 40000-40003 addresses internally
	    # because of the function code.  So setting the target address to 0 in
	    # software will cause a write to register 40000.
	    set response [::modbus::format_message $Station_number 16 $modbus_reg 1 $voltage_hex $Timeout_ms]
	    if {$response eq "ok"} {
		${log}::info "Set channel $voltage_channel to $voltage_V V (0x${voltage_hex})"
		lset Output_voltage_list $modbus_reg $voltage_V
	    } else {
		${log}::error "Failed to set channel $voltage_channel to $voltage_V V (0x${voltage_hex})"
	    }

	    return $response
	}

	method get_hardware_voltage {voltage_channel} {
	    # Read voltage setting back from the hardware
	    global log
	    variable Channel
	    variable Station_number
	    variable Timeout_ms

	    # Modbus addresses start at 0 instead of 1
	    set modbus_reg [expr $voltage_channel -1]
	    set response [::modbus::format_message $Station_number 3 $modbus_reg 1 $Timeout_ms]
	    if {$response ne "timeout"} {
		set output_hex $response
	    } else {
		${log}::error "Read from station $Station_number failed after $Timeout_ms ms"
		return "error"
	    }
	    set output_dec [expr 0x$output_hex]
	    set output_voltage [expr ($output_dec / double(50000)) * 10]
	    return $output_voltage
	}

	method set_remembered_voltage {voltage_channel setting_v} {
	    variable Output_voltage_list
	    set list_index [expr $voltage_channel - 1]
	    lset Output_voltage_list $list_index $setting_v
	}

	method get_remembered_voltage { voltage_channel } {
	    # Get the last voltage set to the channels.  This speeds
	    # up root window updates by not forcing widgets to query
	    # the actual hardware.
	    variable Output_voltage_list
	    set list_index [expr $voltage_channel - 1]
	    set voltage [lindex $Output_voltage_list $list_index]
	    return $voltage
	}

	method get_channel {} {
	    variable Channel
	    return $Channel
	}

	method get_baud {} {
	    variable Baud
	    return $Baud
	}

	method get_parity {} {
	    variable Parity
	    return $Parity
	}

	destructor {
	    usb::close_channel $Channel
	}
    }
}
