
package require Tk

# The root window needs LEDs
main::source_script leds.tcl

# For the help menu
main::source_script help.tcl

# We need the svg module for buttons
lappend auto_path lib
package require tksvg
${log}::info [main::modinfo tksvg]

# Need tooltips for help when hovering over things
package require tooltip
${log}::info [main::modinfo tooltip]

# The tooltips require textutil for wrapping text
package require textutil
${log}::info [main::modinfo textutil]

namespace eval root_window {

    # The Approved vendor ID found during USB device discovery
    variable found_vendor_id ""

    # The vendor name corresponding to the approved vendor ID
    variable found_vendor_name ""

    # The virtual COM port associated with the discovered approved device
    variable found_vcp ""

    # The station number (defaults to 1)
    variable station_number 1

    # The channel voltage settings
    foreach channel [iterint 1 4] {
	variable channel_${channel}_setting_v 0
    }

    proc populate_vcps {} {
	global log
	set vcp_dict [::usb::detect_vcps]
	set found_vendor_id ""
	foreach {vcp} [dict keys $vcp_dict] {
	    set vid [dict get $vcp_dict $vcp vendor_id]
	    set pid [dict get $vcp_dict $vcp product_id]
	    set index [dict get $vcp_dict $vcp composite_index]
	    # Look through the approved device list for a match
	    foreach device $::usb::approved_device_list {
		if {[string match -nocase [$device get_vendor_id] $vid] &&
		    [string match -nocase [$device get_product_id] $pid]} {
		    # We found an approved device.  Populate the root window fields
		    set ::root_window::found_vendor_id $vid
		    set ::root_window::found_vcp $vcp
		    set ::root_window::found_vendor_name [$device get_vendor_name]

		    # We can only work with one device.  Stop after
		    # finding the first approved device.
		    return
		}
	    }

	}
	# If we made it here, there were no approved devices found.
	${log}::error "No approved USB devices found"
    }

    proc create_station {} {
	global log
	set node [::usb::format_vcp_node $::root_window::found_vcp]
	try {
	    set channel [open $node r+]
	} trap {} {message optdict} {
	    # Trap the usage signal, print the message, and exit the application.
	    # Note: Other errors are not caught and passed through to higher levels!
	    ${log}::error $message
	    # We couldn't open the channel, which means there's something wrong with the node
	    set ::station::active_station ""
	    return
	}
	${log}::debug "Trying to configure $node"
	update idletasks

	# Create the rtu307c object in the top-level namespace
	set ::rtu307c [::rtu307c::RTU307C new $channel]
	chan configure $channel -mode "[$::rtu307c get_baud],[$::rtu307c get_parity],8,1" \
	    -buffering none \
	    -blocking 0 \
	    -translation $usb::command_translation
	${log}::debug "Channel configured using $::root_window::found_vcp"

	# Confirm that this is a good device by reading all the outputs
	foreach voltage_channel [iterint 1 4] {
	    # set response [request_voltage_settings $voltage_channel]
	    set response [$::rtu307c get_hardware_voltage $voltage_channel]
	    if {$response ne "error"} {
		# This is a valid voltage.  Make it the most recent voltage setting.
		$::rtu307c set_remembered_voltage $voltage_channel $response
		update_voltage_widgets $voltage_channel
		continue
	    } else {
		${log}::error "Could not communicate with device at $::root_window::found_vcp, station $::root_window::station_number"
		$::connection_status_led set_color "red"
		return
	    }
	    update idletasks
	}
	$::connection_status_led set_color "green"
	$::sync_led set_color green
	# Create bindings for setting voltages
	#
	# See
	# https://wiki.tcl-lang.org/page/Bindings+and+variable+substitution
	# for a description of why we can't use curly braces with bind here.
	foreach channel [iterint 1 4] {
	    set voltage_entry .outputs_frame.sliders_frame.vset_${channel}_frame.channel_${channel}_entry
	    bind $voltage_entry <Return> [list root_window::request_voltage_settings $channel]
	}
    }

    proc destroy_station {} {
	# Called by the disconnect button
	global log
	try {
	    $::rtu307c destroy
	} trap {} {message optdict} {
	    ${log}::debug "$message"
	}
	$::connection_status_led set_color "red"
	$::sync_led set_color red

	# Clear serial transceiver details -- the disconnect button
	# clears everything.
	set ::root_window::found_vendor_id ""
	set ::root_window::found_vcp ""
	set ::root_window::found_vendor_name ""
    }

    proc request_voltage_settings {voltage_channel} {
	# Call the rtu307c's voltage setting function using the root
	# window's voltage setting for a given channel.
	global log
	$::sync_led set_color "red"
	update idletasks

	# Use set here because I have to do double variable substitution
	set voltage [set ::root_window::channel_${voltage_channel}_setting_v]
	set response "error"

	try {
	    set response [$::rtu307c set_voltage $voltage_channel $voltage]
	    update_voltage_widgets $voltage_channel
	    $::sync_led set_color green
	} trap {} {message optdict} {
	    ${log}::debug "$message"
	}
	return $response
    }

    proc update_voltage_widgets {voltage_channel} {
	# Update widgets that display current voltages with values
	# from the object
	set voltage [$::rtu307c get_remembered_voltage $voltage_channel]
	set ::root_window::channel_${voltage_channel}_setting_v $voltage

	# Update the slider position with value from the object
	set ::root_window::channel_${voltage_channel}_scale $voltage
    }

    # Create procs for each slider (scale) to call.  The foreach loop
    # allows us to write the proc once and duplicate it for each channel.
    foreach voltage_channel [iterint 1 4] {
	proc set_voltage_${voltage_channel}_from_scale {scale_value} {
	    # Update the voltage setting entry from the scale widget
	    # and call the request_voltage_settings procedure with
	    # that new value.

	    # Get the channel number from the name of this procedure
	    set info_string [info level 0]
	    set voltage_channel [lindex [split $info_string "_"] 3]
	    set ::root_window::channel_${voltage_channel}_setting_v [format "%0.2f" $scale_value]
	    if [info exists ::rtu307c] {
		request_voltage_settings $voltage_channel
	    }

	}
    }

    proc move_slider_with_wheel {window increment} {
	# Called when the mouse hovers over a slider and the mousewheel moves
	set scaled_increment [expr $increment/120]
	if {$scaled_increment == 1} {
	    event generate $window <Left>
	} else {
	    event generate $window <Right>
	}
    }

    proc exit_application {} {
	exit
    }

}

# Set window name
wm title . "$main::program_name"

# menubar
menu .menubar
. configure -menu .menubar -height 150

# File menu item
menu .menubar.file -tearoff 0
.menubar add cascade -label "File" -menu .menubar.file
.menubar.file add command -label "Exit" \
    -underline 0 -command ::root_window::exit_application

# Add help menu item
menu .menubar.help -tearoff 0
.menubar add cascade -label "Help" -menu .menubar.help
.menubar.help add command -label "About [string totitle $main::program_name...]" \
    -underline 0 -command ::help::about

# The Stations frame
ttk::label .stations_frame_label \
    -text "Stations" \
    -font fonts::frame_font

ttk::labelframe .stations_frame \
    -labelwidget .stations_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief solid

# The search for hardware button
image create photo search_photo -file svg/system-search.svg \
    -format "svg -scaletoheight $main::button_size"
button .stations_frame.search_button -image search_photo \
    -command root_window::populate_vcps
set tooltip "Search for approved RS-232 or RS-485 transceivers"
tooltip::tooltip .stations_frame.search_button \
    [textutil::adjust $tooltip -length 30]

# The connect to hardware button
image create photo arrow_photo -file svg/go-next.svg \
    -format "svg -scaletoheight $main::button_size"
button .stations_frame.connect_button -image arrow_photo \
    -command root_window::create_station
set tooltip "Connect to discovered RS-232 or RS-485 device"
tooltip::tooltip .stations_frame.connect_button \
    [textutil::adjust $tooltip -length 30]

# The port entry label
ttk::label .stations_frame.port_label \
    -font fonts::frame_font \
    -text "Port"

# The read-only port entry
ttk::entry .stations_frame.port_entry \
    -textvariable ::root_window::found_vcp \
    -width 12 \
    -justify center \
    -font fonts::settings_font \
    -state readonly

# The vendor ID entry label
ttk::label .stations_frame.vendor_id_label \
    -font fonts::frame_font \
    -text "Vendor ID"

# The read-only vendor ID entry
ttk::entry .stations_frame.vendor_id_entry \
    -textvariable ::root_window::found_vendor_id \
    -width 8 \
    -justify center \
    -font fonts::settings_font \
    -state readonly

# The vendor name entry label
ttk::label .stations_frame.vendor_name_label \
    -font fonts::frame_font \
    -text "Vendor"

# The read-only vendor name entry
ttk::entry .stations_frame.vendor_name_entry \
    -textvariable ::root_window::found_vendor_name \
    -width 12 \
    -justify center \
    -font fonts::settings_font \
    -state readonly

# The station entry label
ttk::label .stations_frame.station_label \
    -font fonts::frame_font \
    -text "Station"

# The station number entry
ttk::entry .stations_frame.station_entry \
    -textvariable ::root_window::station_number \
    -width 8 \
    -justify center \
    -font fonts::settings_font
set tooltip "The station number. "
append tooltip "This is sometimes set with DIP switches on the hardware."
tooltip::tooltip .stations_frame.station_entry \
    [textutil::adjust $tooltip -length 30]

# The connection OK LED
set connection_status_led [leds::LED new .stations_frame "Status"]
[$connection_status_led get_label] configure -font fonts::frame_font

# The disconnect from hardware button
image create photo stop_photo -file svg/process-stop.svg \
    -format "svg -scaletoheight $main::button_size"
button .stations_frame.disconnect_button -image stop_photo \
    -command root_window::destroy_station
set tooltip "Disconnect from station and RS-232 / RS-485 devices"
tooltip::tooltip .stations_frame.disconnect_button \
    [textutil::adjust $tooltip -length 30]

# The outputs frame
ttk::label .outputs_frame_label \
    -text "Outputs" \
    -font fonts::frame_font

ttk::labelframe .outputs_frame \
    -labelwidget .outputs_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief solid

# The synchronization status LED
ttk::frame .outputs_frame.sync_frame -borderwidth 10
set sync_led [leds::LED new .outputs_frame.sync_frame "Synchronized"]
[$sync_led get_label] configure -font fonts::frame_font

# The voltage sliders frame
ttk::frame .outputs_frame.sliders_frame

foreach channel [iterint 1 4] {
    ttk::frame .outputs_frame.sliders_frame.vset_${channel}_frame

    # Manual voltage entry
    ttk::entry .outputs_frame.sliders_frame.vset_${channel}_frame.channel_${channel}_entry \
	-textvariable ::root_window::channel_${channel}_setting_v \
	-width 8 \
	-justify center \
	-font fonts::settings_font
    tooltip::tooltip .outputs_frame.sliders_frame.vset_${channel}_frame.channel_${channel}_entry \
	[textutil::adjust \
	     "Enter voltage and press <Enter> to set voltage manually." \
	     -length 30]
    ttk::label .outputs_frame.sliders_frame.vset_${channel}_frame.unit_label -text "V" \
	-font fonts::settings_font
    set slider [ttk::scale .outputs_frame.sliders_frame.channel_${channel}_scale \
		    -variable ::root_window::channel_${channel}_scale \
		    -length 100 \
		    -orient vertical \
		    -from 10 -to 0 \
		    -command ::root_window::set_voltage_${channel}_from_scale]
    $slider set 0
    ttk::label .outputs_frame.sliders_frame.channel_${channel}_label -text $channel \
	-font fonts::settings_font
    if {[tk windowingsystem] eq "x11"} {
	# See https://wiki.tcl-lang.org/page/mousewheel for this hack under Linux
	bind all <Button-4> \
	    {event generate [focus -displayof %W] <MouseWheel> -delta  120}
	bind all <Button-5> \
	    {event generate [focus -displayof %W] <MouseWheel> -delta -120}
    }
    # Focus on the slider when the mouse comes over it
    bind $slider <Enter> [list focus %W]
    # Focus out the slider when the mouse leaves
    bind $slider <Leave> [list focus .]
    bind $slider <MouseWheel> [list root_window::move_slider_with_wheel %W %D]
}

# The Log frame
ttk::label .log_frame_label \
    -text "Log" \
    -font fonts::frame_font

ttk::labelframe .log_frame \
    -labelwidget .log_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief solid

text .log_frame.text -yscrollcommand {.log_frame.scroll set} \
    -width 80 \
    -height 10 \
    -font fonts::log_font

scrollbar .log_frame.scroll -orient vertical -command {.log_frame.text yview}

######################## Position everything #########################

# Column 0 will change size as the window is resized
grid columnconfigure . 0 -weight 1

set root_window_row 0
set led_row_limit 5

# Stations frame
incr root_window_row
grid config .stations_frame \
    -column 0 \
    -row $root_window_row \
    -columnspan 1 -rowspan 1 \
    -padx $main::widget_padding -pady $main::widget_padding \
    -sticky "snew"

set frame_column 0
grid columnconfigure .stations_frame $frame_column -weight 1
grid config .stations_frame.search_button -column $frame_column -row 1 \
    -padx $main::widget_padding -pady [list 0 $main::widget_padding]

incr frame_column
grid columnconfigure .stations_frame $frame_column -weight 1
grid config .stations_frame.connect_button -column $frame_column -row 1 \
    -padx $main::widget_padding -pady [list 0 $main::widget_padding]

incr frame_column
grid config .stations_frame.port_entry -column $frame_column -row 1 \
    -padx $main::widget_padding -pady 0
grid config .stations_frame.port_label -column $frame_column -row 0 \
    -padx $main::widget_padding -pady 0

incr frame_column
grid columnconfigure .stations_frame $frame_column -weight 1
grid config .stations_frame.vendor_id_entry -column $frame_column -row 1 \
    -padx $main::widget_padding -pady 0
grid config .stations_frame.vendor_id_label -column $frame_column -row 0 \
    -padx $main::widget_padding -pady 0

incr frame_column
grid columnconfigure .stations_frame $frame_column -weight 1
grid config .stations_frame.vendor_name_entry -column $frame_column -row 1 \
    -padx $main::widget_padding -pady 0
grid config .stations_frame.vendor_name_label -column $frame_column -row 0 \
    -padx $main::widget_padding -pady 0

incr frame_column
grid columnconfigure .stations_frame $frame_column -weight 1
grid config .stations_frame.station_entry -column $frame_column -row 1 \
    -padx $main::widget_padding -pady 0
grid config .stations_frame.station_label -column $frame_column -row 0 \
    -padx $main::widget_padding -pady 0

incr frame_column
grid columnconfigure .stations_frame $frame_column -weight 1
grid config [$connection_status_led get_canvas] -column $frame_column -row 1 \
    -padx $main::widget_padding -pady 0
grid config [$connection_status_led get_label] -column $frame_column -row 0 \
    -padx $main::widget_padding -pady 0

incr frame_column
grid columnconfigure .stations_frame $frame_column -weight 1
grid config .stations_frame.disconnect_button -column $frame_column -row 1 \
    -padx $main::widget_padding -pady [list 0 $main::widget_padding]

# Outputs frame
incr root_window_row
grid config .outputs_frame \
    -column 0 \
    -row $root_window_row \
    -columnspan 1 -rowspan 1 \
    -padx $main::widget_padding -pady $main::widget_padding \
    -sticky "snew"

# Synchronization frame inside outputs frame
#
# The sync_frame groups the LED with its label.  This frame will be in
# column 0 of the outputs frame, which should resize with the window.
grid config .outputs_frame.sync_frame \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $main::widget_padding -pady $main::widget_padding

grid columnconfigure .outputs_frame 0 -weight 1

# Synchronized LED
grid config [$sync_led get_canvas] -column 0 -row 0 \
    -padx $main::widget_padding -pady 0
grid config [$sync_led get_label] -column 1 -row 0 \
    -padx $main::widget_padding -pady 0

grid columnconfigure .outputs_frame.sync_frame 1 -weight 1

# Voltage sliders frame inside outputs frame
grid config .outputs_frame.sliders_frame \
    -column 0 \
    -row 1 \
    -columnspan 1 -rowspan 1 \
    -padx $main::widget_padding -pady $main::widget_padding \
    -sticky "snew"

foreach channel [iterint 1 4] {
    grid config .outputs_frame.sliders_frame.vset_${channel}_frame \
	-column [expr $channel -1] -row 0
    grid columnconfigure .outputs_frame.sliders_frame [expr $channel - 1] -weight 1
    grid config .outputs_frame.sliders_frame.vset_${channel}_frame.channel_${channel}_entry \
	-column 0 -row 0
    grid config .outputs_frame.sliders_frame.vset_${channel}_frame.unit_label \
	-column 1 -row 0 -padx [list 5 0]
    grid config .outputs_frame.sliders_frame.channel_${channel}_scale \
	-column [expr $channel -1] -row 1 -pady 10
    grid config .outputs_frame.sliders_frame.channel_${channel}_label \
	-column [expr $channel -1] -row 2 -pady 3
}

# Log frame
incr root_window_row
grid config .log_frame \
    -column 0 \
    -row $root_window_row \
    -columnspan 1 -rowspan 1 \
    -padx $main::widget_padding -pady $main::widget_padding \
    -sticky "snew"

pack .log_frame.scroll -fill y -side right
pack .log_frame.text -fill x -side bottom -fill both -expand true

# Allow the log frame and text widget to expand
grid rowconfigure . $root_window_row -weight 1
grid rowconfigure .log_frame 0 -weight 1
grid columnconfigure .log_frame 0 -weight 1

# Wait for log window to be visible.  This will then be ready to
# accept log messages.
tkwait visibility .log_frame.text

# Exit when the window is closed
bind . <Destroy> exit
