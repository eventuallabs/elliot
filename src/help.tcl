
namespace eval help {
    proc about {} {
	tk_messageBox -message "[string totitle $::main::program_name]\nVersion $::main::program_version" \
	    -title "About [string totitle $::main::program_name]"
    }
}
