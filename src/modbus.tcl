# See https://wiki.tcl-lang.org/page/Modbus+PLC+Communications+Driver

# Cyclic redundancy check
package require crc16


namespace eval modbus {

    proc send_message {message response_size_hex_chars timeout_ms} {
	global log
	set timeoutctr 0
	set reply ""
	# set channel $usb::command_channel
	set channel [$::rtu307c get_channel]
	flush $channel
	set junk [read $channel]
	puts -nonewline $channel $message
	# There must be a silent time after sending the message.  This
	# tells the server that the message has ended.  This time must
	# be at least 3.5 characters, so it will vary with the baud rate.
	after 10
	flush $channel
	while {[string bytelength $reply] < $response_size_hex_chars && $timeout_ms >= $timeoutctr }  {
	    binary scan [read $channel] H* asciihex
	    if {$asciihex != ""} {
		append reply $asciihex
		${log}::debug "Reply is $reply"
	    }
	    after 5
	    incr timeoutctr 5
	}
	if {$timeout_ms < $timeoutctr} {
	    set reply "timeout"
	}
	return $reply
    }

    proc format_message {args} {
	# Format and send a message
	#
	# station -- Address of the server (sometimes set with DIP switches)
	# function -- Function code
	# startreg -- Starting register
	# value_count -- Number of registers or bits to write or read
	# data_hex_chars -- Data you want to send formatted as hex characters
	global log
	if {[llength $args] == 6} {
	    foreach {station function startreg value_count data_hex_chars timeout_ms} $args {}
	} else {
	    foreach {station function startreg value_count timeout_ms} $args {}
	}

	switch $value_count {
	    on                {set value_count 65280}
	    off {set value_count 0}
	}

	append message  [binary format c $station]
	append message  [binary format c $function]
	if {$startreg != {}} {append message  [binary format S $startreg]}

	switch $function {
	    12  {
		# Get communication event log (serial only)
		if {$value_count != {}} {
		    # Format string S specifies formatting 16-bit
		    # integers using big-endian byte order
		    append message  [binary format S $value_count]
		}
	    }
	    6   {
		# Write single holding register
		#
		# Format string H specifies formatting hexadecimal
		# digits using big-endian byte order
		append message  [binary format H* $value_count]
	    }
	    15  -
	    16  {
		# Write multiple coils (15) or holding registers (16)
		#
		# Format string S specifies formatting 16-bit
		# integers using big-endian byte order
		append message  [binary format S $value_count]
		# Format string c specifies formatting 8-bit integers
		append message  [binary format c [expr [string length $data_hex_chars] /2]]
		# Format string H specifies formatting hexadecimal
		# digits using big-endian byte order
		append message  [binary format H* $data_hex_chars]
	    }
	    22  {
		# Mask write register
		# not available in DL05/06
		#
		# Format string H specifies formatting hexadecimal
		# digits using big-endian byte order
		append message  [binary format H* $value_count]
		append message  [binary format H* $data_hex_chars]
	    }
	    default  {
		if {$value_count != {}} {
		    # Format string S specifies formatting 16-bit
		    # integers using big-endian byte order
		    append message  [binary format S $value_count]
		}
	    }
	}

	set checksum [::crc::crc16 -seed 0xFFFF $message]

	# Format string s specifies formatting 16-bit integers using
	# little-endian format
	set checksum [binary format s $checksum]
	append message $checksum
	binary scan $message H* hex_message
	${log}::debug "Outgoing message is $hex_message"
	switch $function {
	    1  -
	    2  {
		# Read coils (1) or read discrete inputs (2)
		#
		# Calculated reponse sizes are in number of hex
		# characters.  1 byte is two hex characters, so we get
		# these 2x multipliers everywhere.
		#
		# Each hex character is 4 bits, so it takes 4 points
		# to make one hex character.
		set response_size_hex_chars [expr $value_count / 4 +5 *2]
	    }
	    3  {
		# Read multiple holding registers
		#
		# Each register is 16-bits, or four hex characters.
		set response_size_hex_chars [expr $value_count * 4 +5 *2]
	    }
	    4  {
		# Read input registers
		#
		# Each register is 16-bits, or four hex characters
		set response_size_hex_chars [expr $value_count * 4 +5 *2]
	    }
	    default {set response_size_hex_chars 12}
	}
	set result [modbus::send_message $message $response_size_hex_chars $timeout_ms]
	if {$result == "timeout"} {
	    return $result
	}
	if {[string index $result 2] <= 7} {
	    # This is the hex character corresponding to the upper 4
	    # bits of the function code.
	    if {$function <= 4} {
		# Functions <= 4 are read functions.  The read result
		# will start at byte 3, or at 6 hex characters.
		set result [string range $result 6 end-4]
	    } else {
		set result "ok"
	    }
	} else {
	    set result "error"
	}

	return $result
    }
    
}
