
namespace eval main {

    # The name of this program.  This will get used to identify logfiles,
    # configuration files and other file outputs.
    variable program_name elliot

    # The program version
    variable program_version "1.0.0"

    # The standard padding around widgets
    variable widget_padding 5

    # The standard button size (pixels)
    variable button_size 32


    proc modinfo {modname} {
	# Return loaded module details.
	set modver [package require $modname]
	set modlist [package ifneeded $modname $modver]
	set modpath [lindex $modlist end]
	return "Loaded $modname module version $modver from ${modpath}."
    }

    proc source_script {file_name} {
	global log
	# Only source a script if it doesn't already exist
	#
	# Tool files must create namespaces named after their file names
	if {![namespace exists [file rootname $file_name]]} {
	    # The window doesn't exist yet.  Source it from the top level
	    uplevel #0 "source $file_name"
	} else {
	    raise .[file rootname $file_name]
	    ${log}::error "[file rootname $file_name] already exists"
	}
    }
}

# --------------------- Global configuration --------------------------

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name elliot

# The base filename for the execution log.  The actual filename will add
# a number after this to make a unique logfile name.
set execution_logbase "elliot"

# Set the log level.  Known values are:
# debug
# info
# notice
# warn
# error
# critical
# alert
# emergency
set loglevel debug

set root_directory [file dirname $argv0]

# ---------------------- Command line parsing -------------------------
package require cmdline
set usage "usage: [file tail $argv0] \[options]"

set options {
}

try {
    array set params [cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $msg
    exit 1
}


# --------------------- Tools for code modules ------------------------
source [file join $root_directory module_tools.tcl]

# Math tools
source [file join $root_directory jpmath.tcl]

#----------------------------- Set up logger --------------------------

# The logging system will use the console text widget for visual
# logging.

package require logger
main::source_script lologger.tcl
${log}::info [modinfo logger]


proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

# Support for finding and connecting to USB devices
source usb.tcl

# Provides approved_device_list for USB devices
source approved_usb_devices.tcl


source root_window.tcl


# Testing the logger

puts "Current loglevel is: [${log}::currentloglevel]"
${log}::info "Trying to log to $lologger::log_file"
${log}::info "Known log levels: [logger::levels]"
${log}::info "Known services: [logger::services]"
${log}::debug "Debug message"
${log}::info "Info message"
${log}::warn "Warn message"


source modbus.tcl
source rtu307c.tcl

source stations.tcl

# usb::open_command_channel
# 
# set steps 1
# set apex_V 8.5
# set step_size_V [expr double($apex_V)/$steps]
# set step_dwell_ms 1000
# rtu307c::set_voltage 0 0
# 
# foreach step [iterint 1 $steps] {
#     set step_V [expr $step * $step_size_V]
#     rtu307c::set_voltage 0 $step_V
#     after $step_dwell_ms
# }
# 
# foreach step [iterint 1 $steps] {
#     set step_V [expr $apex_V - $step * $step_size_V]
#     rtu307c::set_voltage 0 $step_V
#     after $step_dwell_ms
# }
# 
# 
# 
# 
# close $usb::command_channel

