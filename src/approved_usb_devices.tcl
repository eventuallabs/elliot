
# Add USB devices here with arguments according to the USB device constructor:
#
# usb::Device new <Vendor ID> <Vendor Name> <Product ID> <Product Name>
#
# You can use lsusb under Linux and the Device Manager under Windows
# to find these values for your device.  You can also use Findcoms:
#
# https://gitlab.com/eventuallabs/findcoms


# You can use the list of USB IDs at:
#
# http://www.linux-usb.org/usb.ids
#
# ...to identify your device.

# Note that Elliot will connect to the first approved device found in
# your system.  You can remove devices from this file or physically
# remove them to prevent connections.

# QinHeng USB/RS-485 adaptors have VID = 0x1A86 and PID = 5523
lappend usb::approved_device_list [usb::Device new 1a86 "QinHeng" 5523 "USB/RS-485 adaptor"]

# QinHeng USB/RS-232 adaptors have VID = 0x1A86 and PID = 7523
lappend usb::approved_device_list [usb::Device new 1a86 "QinHeng" 7523 "USB/RS-232 adaptor"]
