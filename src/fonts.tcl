
# Fonts don't use namespaces, but I need the namespace to keep this
# from being sourced more than once.
namespace eval fonts {

    # LED labels
    font create fonts::led_font -family TkDefaultFont -size 20

    # Labels for frames
    font create fonts::frame_font -family TkDefaultFont -size 12

    # Settings entries (like port, station, vendor ID)
    font create fonts::settings_font -family TkDefaultFont -size 12

    # On-screen log font
    font create fonts::log_font -family TkFixedFont -size 12
}


# fonts for the console logger.
font create FixedFont -family {DejaVu Sans Mono} -size 12

# Font for the terminal
font create terminal_font -family {DejaVu Sans Mono} -size 12

# Font for names of things
font create NameFont -family {Arial} -size 15 -weight bold

# Font for console log
font create log_font -family {DejaVu Sans Mono} -size 8

# Big font for test results
font create ShoutFont -family TkFixedFont -size 30

# Font for counters
font create counter_font -family TkFixedFont -size 10

# Font for column headers
font create header_font -family Arial -size 12 -weight bold

# Font for frame labels
font create frame_font -family Arial -size 12

# Fonts for short operator entry
font create big_entry_font -family {Arial} -size 20

# Fonts for big fixed-width
font create big_fixed_font -family {DejaVu Sans Mono} -size 20
