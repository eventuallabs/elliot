namespace eval usb {

    # The command channel virtual COM port
    variable command_vcp ""
    variable command_channel ""

    # Serial port parity settings:
    #
    # n -- none
    # o -- odd
    # e -- even
    # m -- mark
    # s -- space
    variable command_parity n

    variable command_translation binary

    # A USB device with things like vendor and product IDs
    oo::class create Device {

	# The vendor ID
	variable Vendor_id

	# The vendor name
	variable Vendor_name

	# The product ID
	variable Product_id

	# The product name
	variable Product_name

	# Approved for use?
	variable Approved

	constructor {vendor_id vendor_name product_id product_name} {
	    set Vendor_id $vendor_id
	    set Vendor_name $vendor_name
	    set Product_id $product_id
	    set Product_name $product_name

	    # Devices are approved when they are created
	    set Approved true
	}

	method get_vendor_id {} {
	    variable Vendor_id
	    return $Vendor_id
	}

	method get_product_id {} {
	    variable Product_id
	    return $Product_id
	}

	method get_vendor_name {} {
	    variable Vendor_name
	    return $Vendor_name
	}
    }

    proc detect_vcps {} {
	# Returns a dictionary of:
	#
	# (COM port or filesystem node): vendor_id, product_id, composite_index
	global log
	set vcp_dict [dict create]
	switch $::tcl_platform(os) {
	    {Linux} {
		set nodelist ""
		try {
		    # FTDI devices show up as ttyUSB*
		    set nodelist [glob -nocomplain -directory /dev/ ttyUSB*]
		    # Arduino devices show up as ttyACM*
		    lappend nodelist [glob -nocomplain -directory /dev/ ttyACM*]
		} trap {} {message optdict} {
		    ${log}::debug $message
		    set nodelist ""
		}
		foreach vcp [join $nodelist] {
		    try {
			set udev_data [exec udevadm info --query=property --name $vcp]
			set id_serial ""
			set id_usb_interface_number ""
			foreach line [split $udev_data "\n"] {
			    if {[string first "ID_SERIAL=" $line] >= 0} {
				set id_serial [lindex [split $line "= \n"] 1]
			    }
			    if {[string first "ID_USB_INTERFACE_NUM=" $line] >= 0} {
				set composite_index [string trimleft [lindex [split $line "= \n"] 1] 0]
			    }
			    if {[string first "ID_VENDOR_ID=" $line] >= 0} {
				set vendor_id [string trimleft [lindex [split $line "= \n"] 1] 0]
			    }
			    if {[string first "ID_MODEL_ID=" $line] >= 0} {
				set product_id [string trimleft [lindex [split $line "= \n"] 1] 0]
			    }

			}
		    } trap CHILDSTATUS {results options} {
			${log}::debug "oops -- $results"
			set status [lindex [dict get $options -errorcode] 2]
			continue
		    }
		    ${log}::debug "ID_SERIAL is $id_serial"
		    ${log}::debug "  Vendor ID: $vendor_id"
		    ${log}::debug "  Product ID: $product_id"
		    ${log}::debug "  COM port: $vcp"
		    ${log}::debug "  Composite index: $composite_index"
		    dict set vcp_dict $vcp vendor_id $vendor_id
		    dict set vcp_dict $vcp product_id $product_id
		    dict set vcp_dict $vcp composite_index $composite_index
		}
	    }
	    {Windows NT} {
		# Grab the device info set for present objects only
		set dfs [twapi::devinfoset -presentonly true]
		foreach element [twapi::devinfoset_elements $dfs] {
		    catch {twapi::devinfoset_element_registry_property $dfs $element class} classname
		    if {$classname eq "Ports"} {
			# USB/serial ports are part of the "ports" class
			catch {twapi::devinfoset_element_registry_property $dfs $element friendlyname} friendlyname
			${log}::debug "Friendlyname is $friendlyname"
			# Get the COMn number from the friendlyname.  Friendlynames look like
			#
			# USB Serial Device (COM5)
			set vcp [lindex [split $friendlyname "()"] 1]
			catch {twapi::devinfoset_element_registry_property $dfs $element mfg} mfg
			${log}::debug "  Manufacturer is $mfg"
			catch {twapi::devinfoset_element_registry_property $dfs $element hardwareid} hardwareid_list
			# There may be more than one entry in the hardare ID list.  Just use the first one.
			set hwid [lindex $hardwareid_list 0]
			${log}::debug "  Hardwareid is $hwid"
			set vendor_id [get_windows_hwid_value $hwid "VID_"]
			set product_id [get_windows_hwid_value $hwid "PID_"]
			set composite_index [get_windows_hwid_value $hwid "MI_"]
			${log}::debug "  Vendor ID: $vendor_id"
			${log}::debug "  Product ID: $product_id"
			${log}::debug "  COM port: $vcp"
			${log}::debug "  Composite index: $composite_index"
			dict set vcp_dict $vcp vendor_id $vendor_id
			dict set vcp_dict $vcp product_id $product_id
			dict set vcp_dict $vcp composite_index $composite_index
		    }
		}
		twapi::devinfoset_close $dfs
	    }
	    default {
		# This should never happen
		${log}::debug "Failed to detect OS"
	    }
	}

	return $vcp_dict
    }

    proc test_usb_connection {} {
	global log
	global state_dict
	global acm_node_status_led_canvas
	global bootloader_task_led_canvas
	${log}::info "Looking for CDC-ACM connection"
	set detection_list [detect_ports]
	if {[llength $detection_list] == 0} {
	    ${log}::error "No CDC-ACM ports found"
	    return
	}
	set found_count 0
	foreach node $detection_list {
	    set prefix [lindex [split [lindex $node 1] "-"] 0]
	    if {[string match $prefix ftdi]} {
		incr found_count
		set acm_node [lindex $node 0]
		lappend port_list $acm_node
		${log}::debug "Found FTDI device at $acm_node"
	    }
	}
	if {$found_count == 1} {
	    status_leds::set_led $acm_node_status_led_canvas true
	    dict set state_dict acm_connected true
	    ${log}::info "Setting CDC-ACM node to $acm_node"
	    dict set state_dict acm_node $acm_node
	} else {
	    status_leds::set_led $acm_node_status_led_canvas false
	    dict set state_dict acm_connected false
	}
	test_application
	if {[dict get $state_dict application_flashed]} {
	    # If the application has been flashed, the bootloader is there.
	    status_leds::set_led $bootloader_task_led_canvas true
	    dict set state_dict bootloader_flashed true
	}
    }

    proc get_command_vcp {} {
	# Return the Virtual Com Port for the command interface
	#
	global log
	global state_dict

	set vcp_dict [detect_vcps]
	foreach {vcp} [dict keys $vcp_dict] {
	    ${log}::debug "Found $vcp"
	    set vid [dict get $vcp_dict $vcp vendor_id]
	    set pid [dict get $vcp_dict $vcp product_id]
	    set index [dict get $vcp_dict $vcp composite_index]
	    ${log}::debug "  Vendor ID: 0x$vid"
	    ${log}::debug "  Product ID: 0x$pid"
	    ${log}::debug "  Composite index: $index"
	    if {[lsearch -exact -nocase $usb::command_vid_match_list $vid] >= 0 &&
		[lsearch -exact -nocase $usb::command_pid_match_list $pid] >= 0} {
		${log}::info "Found command interface at $vcp"
		set usb::command_vcp $vcp
		return $vcp
	    }
	}
	# We made it through the loop.  That means we didn't find anything.
	set usb::command_vcp ""
	return ""
    }

    proc format_vcp_node {alias} {
	# Returns a string able to be used by open to open a hardware
	# connection.
	#
	# Arguments:
	#
	# alias -- Short connection name returned by get_potential_aliases
	global log
	global state_dict
	switch $::tcl_platform(os) {
	    {Linux} {
		set nodename $alias
	    }
	    {Windows NT} {
		set nodename \\\\.\\[string toupper $alias]
	    }
	}

	return $nodename
    }

    proc get_windows_hwid_value {hwid delimiter} {
	# Return a value from a windows hardware ID
	#
	# Arguments:
	#   hwid -- Windows hardware ID, like USB\VID_1FFB&PID_00BB&REV_0102&MI_01
	#   delimiter -- Substring like "VID_" or "PID_"

	# Prefix string will start with the value we want terminated in a &
	set prefix_string [lindex [textutil::splitx $hwid $delimiter] 1]
	set value [string trimleft [lindex [split $prefix_string &] 0] 0]
	return $value
    }

    proc close_channel {channel} {
	global log
	global state_dict
	# set channel [dict get $state_dict command_channel]
	if {$channel ne ""} {
	    ${log}::debug "Trying to close channel $channel"
	} else {
	    ${log}::debug "No explicit channel.  Other channels are [chan names]"
	}

	try {
	    ${log}::debug "Closing CDC-ACM channel"
	    chan close $channel
	} trap {} {message optdict} {
	    ${log}::debug "No channel to close"
	    dict set state_dict command_channel ""
	}
	dict set state_dict command_channel ""
    }

    proc wait_for_data {channel chars timeout_ms} {
	# Busy loop until a byte is ready to be read or until timeout
	#
	# Arguments:
	#  channel -- Channel created with open and configured with chan configure
	#  chars -- Number of characters to read
	#  timeout_ms -- Timeout in milliseconds
	#
	# Don't use the event loop for this, since Tcl might decide to
	# slip another channel transaction in during the wait --
	# screwing up return values.
	global log
	set start_time_ms [clock milliseconds]
	set channel_data ""
	while {! [string length $channel_data]} {
	    try {
		set channel_data [chan read $channel $chars]
	    } trap {} {message optdict} {
		${log}::error "Error reading from channel: $message"
	    }
	    set elapsed_time_ms [expr [clock milliseconds] - $start_time_ms]
	    if {$elapsed_time_ms >= $timeout_ms} {
		${log}::debug "Listened to channel for $timeout_ms ms and got no data"
		return
	    }
	}
	return $channel_data
    }

    proc read_until_timeout {channel timeout_ms} {
	# Read all data from the channel until there's a gap lasting timeout_ms
	#
	# Arguments:
	#  channel -- Channel created with open and configured with chan configure
	#  timeout_ms -- Timeout in milliseconds
	global log
	set start_time_ms [clock milliseconds]
	set times_up false
	set channel_data ""
	while {! $times_up} {
	    set new_data [chan read $channel]
	    set elapsed_time_ms [expr [clock milliseconds] - $start_time_ms]
	    if { [string length $new_data] } {
		# We got new data, reset the timer
		${log}::debug "New data at $elapsed_time_ms ms"
		update idletasks
		set start_time_ms [clock milliseconds]
		append channel_data $new_data
	    }

	    if {$elapsed_time_ms >= $timeout_ms} {
		set times_up true
	    }
	}
	return $channel_data
    }

    proc open_command_channel {} {
	global log
	global state_dict
	global application_task_led_canvas
	global acm_node_status_led_canvas
	if {$usb::command_vcp eq ""} {
	    # There's no command CDC-ACM node.  Try looking for one
	    get_command_vcp
	    if {$usb::command_vcp eq ""} {
		# We tried and failed to find a command CDC-ACM node
		${log}::error "No command VCP found"
		return
	    }
	}
	set node [format_vcp_node $usb::command_vcp]
	try {
	    set channel [open $node r+]
	} trap {} {message optdict} {
	    # Trap the usage signal, print the message, and exit the application.
	    # Note: Other errors are not caught and passed through to higher levels!
	    ${log}::error $message
	    # We couldn't open the channel, which means there's something wrong with the node
	    set $usb::command_vcp ""
	    return
	}
	${log}::debug "Trying to configure $node"
	update idletasks
	chan configure $channel -mode "${usb::command_baud},${usb::command_parity},8,1" \
	    -buffering none \
	    -blocking 0 \
	    -translation $usb::command_translation
	${log}::debug "Channel configured using $usb::command_vcp"
	update idletasks
	# Wait for unsolicited data to come through, or timeout if there's no data
	set data [wait_for_data $channel 5 100]
	# Clear out the Rx queue
	append data [read_until_timeout $channel 100]
	${log}::debug "Unsolicited data after boot: [string trim $data]"
	set usb::command_channel $channel
    }

    proc test_application {} {
	global log
	global state_dict
	# The application is flashed if we can open a CDC-ACM channel
	# and it responds appropriately.
	open_acm_channel
    }

    proc get_serial_number {} {
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	${log}::info "Querying serial number..."
	update idletasks
	puts -nonewline $channel "*idn?\r"
	after 500
	set data [chan gets $channel]
	text_console::manager $data
	${log}::debug "Response to *idn? was $data"
	set sernum_string [lindex [split $data ","] 2]
	set sernum [lindex [split $sernum_string "SN"] end]
	${log}::debug "Serial number is $sernum"
	return $sernum
    }

    proc get_firmware_version {} {
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	${log}::info "Querying firmware version..."
	update idletasks
	puts -nonewline $channel "*idn?\r"
	after 500
	set data [chan gets $channel]
	text_console::manager $data
	${log}::debug "Response to *idn? was $data"
	set version_string [lindex [split $data ","] 3]
	${log}::debug "Version number is $version_string"
	return $version_string
    }

}
